@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="alert delete-success"></div>
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    <div class="col-lg-12">
                        <div class="card-body table-responsive">
                            <table class="table table-bordered my_booking_datatable">
                                <thead>
                                    <tr>
                                        <th>Room Name</th>
                                        <th>Booking Date</th>
                                        <th>Booking Slot Time</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
    $(function () {
        var table = $('.my_booking_datatable').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                type: 'GET',
                url: "{{ route('getBooking') }}",
            },
            columns: [
                {data: 'get_room.room_name', name: 'get_room'},
                {data: 'booking_date', name: 'booking_date'},
                {data: 'booking_slot_time', name: 'booking_slot_time'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ],
            destroy: true
        });
    });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    function deleteBooking(id)
    {
        if(id)
        {
            $.ajax({
               url: "{{route('deleteBooking')}}",
               type: 'POST',
               data: {
                id: id
               },
                beforeSend: function()
                {
                    $("#delete_room").html("<span><i class='fa fa-spin fa-spinner'></i></span>");
                },
               success: function(data) {
                $("#delete_room").html("Delete");
                    if(data.success)
                    {
                        $(".delete-success").text(data.message).addClass('alert-success');
                        setTimeout(function() { window.location.reload(); }, 2000);
                    }
                    else
                    {
                        $(".delete-success").text(data.message).addClass('alert-danger');
                    }

               },
               error: function(e) {
                    alert(e.getMessage());
               }
            });
        }
    }
</script>
@endsection