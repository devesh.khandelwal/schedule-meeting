<!-- The Modal -->
<div class="modal" id="addRoom">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Add New Room</h4>
        <button type="button" class="close" data-dismiss="modal" onclick="closeModal();">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <div class="row">
            <div class="form-group">
                <label for="room_name">Room Name</label>
                <input type="text" name="room_name" id="room_name" class="form-control room_name" />
                <span class="error text-danger" id="room_error"></span>
            </div>
            <div class="form-group">
                <label for="max_booking">Max Booking</label>
                <input type="text" name="max_booking" id="max_booking" class="form-control max_booking" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" />
                <span class="error text-danger" id="booking_error"></span>
            </div>
        </div>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer row">
        <p class="col-lg-7 float-left success-msg"></p>
        <button type="button" class="btn btn-primary col-lg-2" name="submit" id="submit">Add</button>
        <button type="button" class="btn btn-danger col-lg-2" data-dismiss="modal" onclick="closeModal();">Close</button>
      </div>
    </div>
  </div>
</div>