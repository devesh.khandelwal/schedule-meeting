<!-- The Modal -->
<div class="modal" id="selectedRoom">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Room</h4>
        <button type="button" class="close" data-dismiss="modal" onclick="closeModal();">&times;</button>
      </div>
      <!-- Modal body -->
      <div class="modal-body">
        <div class="row alert booking-msg">
           
        </div>
        <div class="row">
            <input type="hidden" name="room" id="room">
            <div class="col-lg-3">
                <label for="username">Username</label>
                <input type="text" name="username" class="form-control" id="username" value="{{ ucfirst(Auth::user()->name) }}" readonly>
            </div>
            <div class="col-lg-3">
                <label for="date">booking date</label>
                <input type="text" class="date datepicker form-control" name="booking_date" id="booking_date">
                <p class="text-danger error booking_date_error"></p>
            </div>
            <div class="col-lg-3">
                <label for="time_slot">Time Slot</label>
                <select name="slot" id="slot" class="form-control">
                    <option value="">Select Time Slot</option>
                    @if($slot)
                        @foreach($slot as $value)
                            <option value="{{ str_replace([':00 AM', ':00 PM'], '', $value) }}">{{$value}}</option>
                        @endforeach
                    @endif
                </select>
                <p class="text-danger error slot_error"></p>
            </div>
            <div class="col-lg-3">
                <button type="button" class="btn btn-primary" name="book" id="book" style="margin-top:19%;">Book</button>
            </div>
        </div>
        <div class="row mt-4">
			<div class="card">
            	<div class="card-body table-responsive">
					<table class="table table-bordered room_book_datatable">
						<thead>
							<tr>
								<th>Room Name</th>
								<th>User Name</th>
								<th>Booking Date</th>
								<th>Booking Slot Time</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
        </div>
      </div>
    </div>
  </div>
</div>