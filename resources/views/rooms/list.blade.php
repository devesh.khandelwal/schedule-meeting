@extends('layouts.adminApp')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="row">
            <span class="delete-success alert"></span>
            <div class="card">
                <div class="card-header">
                    <h3 class="float-left"> Rooms </h3>
                    <span class="float-right btn btn-primary" id="add_new_room" data-toggle="modal" data-target="#addRoom" onclick="addFormReset();">Add New Room</span>
                </div>
                <div class="card-body table-responsive">
                    <table class="table table-bordered room_datatable">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Room Name</th>
                                <th>Max Booking</th>
                                <th width="100px">Action</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@include('rooms.create')
@include('rooms.update')
@endsection
@section('scripts')
<script type="text/javascript">
    $(function () {
        addFormReset();
        updateFormReset();
        var table = $('.room_datatable').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('admin.rooms.index') }}",
            columns: [
                {data: 'id', name: 'id'},
                {data: 'room_name', name: 'room_name'},
                {data: 'max_booking', name: 'max_booking'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        });
    });

    function addFormReset() {
        $("#room_name").val('');
        $("#max_booking").val('');
        $("#room_error").text('');
        $("#booking_error").text('');
    }

    function updateFormReset() {
        $("#room_name_update").val('');
        $("#max_booking_update").val('');
        $("#room_update_error").text('');
        $("#booking_update_error").text('');
    }

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    function closeModal()
    {
        addFormReset();
        $("#addRoom, #updateRoom").modal('hide');
        updateFormReset();
    }

    $("#submit").on('click', function(){
        var room_name   = $("#room_name").val();
        var max_booking = $("#max_booking").val();
        var flag = true;
        if(room_name == '')
        {
            flag = false;
            $("#room_error").text('Please enter room name.');
            return false;
        }
        else {
            flag = true;
            $("#room_error").text('');
        }

        if(max_booking == '')
        {
            flag = false;
            $("#booking_error").text('Please enter max booking.');
            return false;
        }
        else {
            flag = true;
            $("#booking_error").text('');
        }

        if(flag)
        {
            $.ajax({
                url: "{{ route('admin.rooms.add') }}",
                type: 'POST',
                data: {
                    room_name: room_name,
                    max_booking: max_booking
                },
                beforeSend: function()
                {
                    $("#submit").html("<span><i class='fa fa-spin fa-spinner'></i></span>");
                },
                success: function(response)
                {
                    $("#submit").html("Add");
                    if(response.success)
                    {
                        $(".success-msg").text(response.message).addClass('text-success');
                    }
                    else
                    {
                        $(".success-msg").text(response.message).addClass('text-danger');
                    }

                    setTimeout(function(){ window.location.reload() }, 2000);
                },
                error: function(e)
                {
                    $("#submit").html("Add");
                    alert(e.getMessage());
                }
            });
        }
    });

    function getDetails(id)
    {
        if(id > 0)
        {
            $.ajax({
               url: "{{ route('admin.rooms.getDetails') }}",
               type: 'POST',
               data: {
                id: id
               },
               success: function(data)
               {
                    if(data)
                    {
                        $("#room_name_update").val(data.room_name);
                        $("#max_booking_update").val(data.max_booking);
                        $("#room_id").val(data.id);

                        $("#updateRoom").modal('show');
                    }
               },
               error: function(e)
               {
                alert(e.getMessage());
               }
            });
        }
    }

    $("#update").on('click', function() {
        var room_name   = $("#room_name_update").val();
        var max_booking = $("#max_booking_update").val();
        var room_id     = $("#room_id").val();

        var flag = true;
        if(room_name == '')
        {
            flag = false;
            $("#room_update_error").text('Please enter room name.');
            return false;
        }
        else {
            flag = true;
            $("#room_update_error").text('');
        }

        if(max_booking == '')
        {
            flag = false;
            $("#booking_update_error").text('Please enter max booking.');
            return false;
        }
        else {
            flag = true;
            $("#booking_update_error").text('');
        }

        if(flag)
        {
            $.ajax({
                url: "{{ route('admin.rooms.update') }}",
                type: 'POST',
                data: {
                    room_name: room_name,
                    max_booking: max_booking,
                    room_id: room_id
                },
                beforeSend: function()
                {
                    $("#update").html("<span><i class='fa fa-spin fa-spinner'></i></span>");
                },
                success: function(response)
                {
                    $("#update").html("Update");
                    if(response.success)
                    {
                        $(".success-msg").text(response.message).addClass('text-success');
                    }
                    else
                    {
                        $(".success-msg").text(response.message).addClass('text-danger');
                    }

                    setTimeout(function(){ window.location.reload() }, 2000);
                },
                error: function(e)
                {
                    $("#submit").html("Update");
                    alert(e.getMessage());
                }
            });
        }
    });

    function deleteRoom(id)
    {
        if(id)
        {
            $.ajax({
                url: "{{ route('admin.room.delete') }}",
                type: 'POST',
                data: {
                    id: id
                },
                beforeSend: function()
                {
                    $("#delete_room").html("<span><i class='fa fa-spin fa-spinner'></i></span>");
                },
                success: function(data) {
                    $("#delete_room").html("Delete");
                    if(data.success)
                    {
                        $(".delete-success").text(data.message).addClass('alert-success');
                    }
                    else
                    {
                        $(".delete-success").text(data.message).addClass('alert-danger');
                    }

                    setTimeout(function() { window.location.reload(); }, 2000);
                },
                error: function(e) {
                    $("#delete_room").html("Delete");
                    alert(e.getMessage());
                }
            });
        }
    }
</script>
@endsection