@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="col-lg-12">
                        <ul class="list-group">
                            @if($data)
                                @foreach($data as $key => $value)
                                    <li class="py-5 list-group-item">
                                        <span class="float-left">{{ ucfirst($value->room_name) }}</span>
                                        <span class="float-right btn btn-primary booking_btn" id="booking_btn" data-toggle="modal" data-target="#selectedRoom" data-room-id="{{ $value->id }}" data-room-name="{{ ucfirst($value->room_name) }}">Join</span>
                                    </li>
                                @endforeach
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('rooms.selected-room')
@endsection
@section('scripts')
<script type="text/javascript">
    $(function () {
            $('#booking_date').datepicker({
                weekStart: 1,
                autoclose: true,
                todayHighlight: true,
                daysOfWeekDisabled: "0,6",
                startDate: new Date()
            });
            $('#booking_date').datepicker("setDate", new Date());
    });

    $(".booking_btn").on('click', function(){
        resetBookForm();
        $('#booking_date').datepicker("setDate", new Date());
        var roomId      = $(this).attr('data-room-id');
        var roomName    = $(this).attr('data-room-name');
        var datatable   = null;
        $(".modal-title").text(roomName);
        $("#room").val(roomId);

        if(roomId)
        {
            var table = $('.room_book_datatable').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    type: 'POST',
                    url: "{{ route('roomBookingList') }}",
                    data: {
                        id: roomId
                    }
                },
                columns: [
                    {data: 'get_room.room_name', name: 'get_room'},
                    {data: 'get_user.name', name: 'get_user'},
                    {data: 'booking_date', name: 'booking_date'},
                    {data: 'booking_slot_time', name: 'booking_slot_time'},
                ],
                destroy: true
            });
        }
    });

    function resetBookForm()
    {
        $("#room").val('');
        $("#booking_date").val('');
        $("#slot").val('');
    }

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $("#book").on('click', function(){
        var username = $("#username").val();
        var roomId   = $("#room").val();
        var bookingDate = $("#booking_date").val();
        var timeSlot    = $("#slot").val();

        var flag = true;

        if(bookingDate == "")
        {
            flag = false;
            $(".booking_date_error").text('Please select booking date.');
            return false;
        }
        else
        {
            $(".booking_date_error").text('');
            flag = true;
        }

        if(timeSlot == "")
        {
            flag = false;
            $(".slot_error").text('Please select time slot.');
            return false;
        }
        else
        {
            $(".slot_error").text('');
            flag = true;
        }

        if(flag)
        {
            $.ajax({
                url: "{{ route('bookRoomSlot') }}",
                type: 'POST',
                data: {
                    username: username,
                    roomId: roomId,
                    booking_date: bookingDate,
                    slot: timeSlot
                },
                beforeSend: function()
                {
                    $("#book").html("<span><i class='fa fa-spin fa-spinner'></i></span>");
                },
                success: function(data) {
                    $("#book").html("Book");
                    if(data.success)
                    {
                        $(".booking-msg").html("<span>"+ data.message +"</span>").addClass("alert-success");
                        setTimeout(function(){ $("#selectedRoom").modal('hide'); window.location.reload();  }, 2000);
                    }
                    else
                    {
                        $(".booking-msg").html("<span>"+ data.message +"</span>").addClass("alert-danger");
                    }

                },
                error: function(e){
                    $("#book").html("Book");
                    alert(e.getMessage());
                }
            });
        }
    });
</script>
@endsection
