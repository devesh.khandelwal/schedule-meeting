

1. Take clone from gitlab by running command in terminal:-

  	git clone "https://gitlab.com/devesh.khandelwal/schedule-meeting.git"
  	
 2. Rename .env.example file to .env
    -- Update database credentails and database name.
    -- Update email credentials.
 
 3. Give permission to storage folder by runnig this command:-
 	
 	sudo chmod -R 777 storage/
 
 4. Update Packages by running this command:-
 	composer install
 	
 5.  Run migration after database credentails update, by running this command:-
 	php artisan migrate
 	
 6. Run the seeder to create 1000 users and one admin by running this commands:-
 	-- php artisan db:seed (for users)
 	-- php artisan db:seed --class=CreateUsersSeeder (for admin)
 	
 7. Now run command to start server:-
 	php artisan serve
 
 8. Now hit the url on browser to access user:-
 	127.0.0.1:8000/
 	Login Credentials:-
 	Email:- (you can choose any user from database)
 	password: 123456
 	
 9.To access Admin on browser:-
 	URL:- 127.0.0.1/admin
 	Login Credentials:-
 	Email:- admin@test.com
 	password:- 12345678
 	
 10. From admin, you can add, edit, delete rooms.
 11. On user panel, you can see the rooms with Join button.
 12. When you click on Join, you can choose date and time slot to book room, if slot is already selected or not availabel then it will shows you an error.
 	

