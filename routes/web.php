<?php

use App\Http\Controllers\Auth\LoginController;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\HomeController;
use App\Http\Controllers\RoomController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', [HomeController::class, 'index'])->name('home');
Route::group(['middleware' => 'auth'], function () {
    Route::post('/bookSlot', [HomeController::class, 'bookRoomSlot'])->name('bookRoomSlot');
    Route::post('/roomBookingList', [HomeController::class, 'roomBookingList'])->name('roomBookingList');
    Route::post('/deleteBooking', [HomeController::class, 'deleteBooking'])->name('deleteBooking');
    Route::get('/getBooking', [HomeController::class, 'getBooking'])->name('getBooking');
});
Route::group(['middleware' => 'auth', 'prefix' => 'admin'], function () {
    Route::get('/', [HomeController::class, 'adminHome'])->name('admin.home')->middleware('is_admin');
    Route::get('/rooms', [RoomController::class, 'index'])->name('admin.rooms.index');
    Route::post('/rooms/add', [RoomController::class, 'addNewRoom'])->name('admin.rooms.add');
    Route::post('/getDetails', [RoomController::class, 'getDetail'])->name('admin.rooms.getDetails');
    Route::post('/rooms/update', [RoomController::class, 'updateRoom'])->name('admin.rooms.update');
    Route::post('/rooms/delete', [RoomController::class, 'deleteRoom'])->name('admin.room.delete');
});