<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Room;
use DataTables;
use App\Http\Requests\AddRoomRequest;
use App\Http\Requests\UpdateRoomRequest;
use Exception;

class RoomController extends Controller
{
    //
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Room::select('id', 'room_name', 'max_booking')->where('deleted_at', null)->get();
            return Datatables::of($data)->addIndexColumn()
                ->addColumn('action', function($row){
                    $btn = '<a href="javascript:void(0)" class="btn btn-primary btn-sm" id="edit" onclick="getDetails('.$row->id.');updateFormReset();">Edit</a> ';
                    $btn .= ' <a href="javascript:void(0)" class="btn btn-default btn-sm" id="delete_room" onclick="deleteRoom('. $row->id .');">Delete</a>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }

        return view('rooms.list');
    }

    public function addNewRoom(AddRoomRequest $request)
    {
        $validated = $request->validated();
        
        try {
            if ($validated) {
                $result = Room::create([
                    'room_name' => trim($validated['room_name']),
                    'max_booking' => trim($validated['max_booking'])
                ]);
            }

            if ($result) {
                $response = array("success" => 1, "message" => "Room added successfully.");
            } else {
                $response = array("error" => 0, "message" => "Error while adding room.");
            }

            return response()->json($response);
        }
        catch(Exception $e)
        {
            print_r($e->getMessage());
            exit;
        }
    }

    public function getDetail(Request $request)
    {
        try {
            return Room::where('id', $request->id)->first()->toArray();
        } catch (Exception $e) {
            echo "<pre>";
            print_r($e->getMessage());
            exit;
        }
    }

    public function updateRoom(UpdateRoomRequest $request)
    {
        $validated = $request->validated();
        try {
            if ($validated) {
                $update = Room::find($request->room_id);
                $update->room_name      = $validated['room_name'];
                $update->max_booking    = $validated['max_booking'];
                $result = $update->save();
            }

            if ($result) {
                $response = array("success" => 1, "message" => "Room updated successfully.");
            } else {
                $response = array("error" => 0, "message" => "Error while updating room.");
            }

            return response()->json($response);
        } catch (Exception $e) {
            echo "<pre>";
            print_r($e->getMessage());
            exit;
        }
    }

    public function deleteRoom(Request $request)
    {
        try {
            $data    = Room::find($request->id)->delete();

            if($data)
                $response = array("success" => 1, "message" => "Room deleted successfully.");
            else
                $response = array("success" => 0, "message" => "Error while deleting room");

            return response()->json($response);
        } catch (Exception $e) {
            echo "<pre>";
            print_r($e->getMessage());
            exit;
        }
    }
}
