<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Room;
use App\Models\RoomBooking;
use Carbon\CarbonPeriod;
use App\Http\Requests\RoomBookRequest;
use Exception;
use Auth;
use DataTables;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $period = new CarbonPeriod('07:00', '60 minutes', '16:00');
        $data = Room::with('bookingId:id')->where('deleted_at', null)->get();

        $slots = [];
        foreach ($period as $item){
            array_push($slots,$item->format("H:i A"));
        }

        return view('home', ["data" => $data, "slot" => $slots]);
    }

        /**

     * Show the application dashboard.
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function adminHome()
    {
        return view('rooms.list');
    }

    public function bookRoomSlot(RoomBookRequest $request)
    {
        $validated = $request->validated();

        try {
            if($validated)
            {
                $slotCheck = RoomBooking::where(['room_id' => $request->roomId, 'booking_date' => date('Y-m-d', strtotime($validated['booking_date'])), 'booking_slot_time' => $request->slot])->get()->toArray();
                
                if(empty($slotCheck))
                {
                    $result = RoomBooking::create([
                        'room_id' => $request->roomId,
                        'user_id' => Auth::id(),
                        'booking_date' => date('Y-m-d', strtotime($validated['booking_date'])),
                        'booking_slot_time' => $request->slot
                    ]);

                    if($result)
                    {
                        $response = array("success" => 1, "message" => "Room Booked successfully.");
                    }
                    else
                    {
                        $response = array("success" => 0, "message" => "Error while booking room");
                    }
                }
                else
                {
                    $response = array("success" => 0, "message" => "This slot not available, please select another slot.");
                }

                return $response;
            }
        } catch (Exception $e) {
            echo "<pre>";
            print_r($e->getMessage());
            exit;
        }
    }

    public function roomBookingList(Request $request)
    {
        try {
            if ($request->id)
            {
                if ($request->ajax()) {
                    $data = RoomBooking::select('*')->with(['getUser:id,name', 'getRoom:id,room_name'])->where('room_id', $request->id)->get();
                    
                    return Datatables::of($data)
                    ->addColumn('booking_slot_time', function($row){
                        $actionBtn = ($row['booking_slot_time'] > 12 ? $row['booking_slot_time']. ":00 PM" : $row['booking_slot_time']. ":00 AM");
                       
                        return $actionBtn;
                    })
                    ->rawColumns(['booking_slot_time'])
                    ->make(true);
                }
                return view('home');
            }
        } catch (Exception $e) {
            echo "<pre>";
            print_r($e->getMessage());
            exit;
        }
    }

    public function getBooking(Request $request)
    {
        if ($request->ajax()) {
            $data = RoomBooking::with(['getUser:id,name', 'getRoom:id,room_name'])->where('user_id', Auth::id())->get();
           
            return Datatables::of($data)->addIndexColumn()
                ->addColumn('booking_slot_time', function ($row) {
                    $actionBtn = ($row['booking_slot_time'] > 12 ? $row['booking_slot_time']. ":00 PM" : $row['booking_slot_time']. ":00 AM");
                
                    return $actionBtn;
                })
                ->rawColumns(['booking_slot_time'])
                ->addColumn('action', function ($row) {
                    $btn = ' <a href="javascript:void(0)" class="btn btn-default btn-sm" id="delete_room" onclick="deleteBooking('. $row->id .');">Delete</a>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }

        return view('myBooking');
    }

    public function deleteBooking(Request $request)
    {
        try {
            $data    = RoomBooking::find($request->id)->delete();

            if ($data)
                $response = array("success" => 1, "message" => "Room Booking deleted successfully.");
            else
                $response = array("success" => 0, "message" => "Error while deleting room booking");

            return response()->json($response);
        } catch (Exception $e) {
            echo "<pre>";
            print_r($e->getMessage());
            exit;
        }
    }
}
