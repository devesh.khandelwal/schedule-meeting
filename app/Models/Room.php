<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'room_name',
        'max_booking'
    ];

    public function bookingId()
    {
        return $this->belongsTo(RoomBooking::class, 'room_id', 'id');
    }
}
